// Ejercicio 2
// Dada una matriz de N elementos repetidos,
// crea una función numbersTop para obtener los tres elementos más repetidos ordenados de forma descendente por número de repeticiones.

function numbersTop(array) {
    const repeticiones = new Map();
    // Contamos el número de apariciones de cada elemento
    array.forEach(element => {
        // Si existe el elemento en repeticiones, le sumamos 1, si no, lo inicializamos a 1.
        if(repeticiones.has(element)){
            repeticiones.set(element, repeticiones.get(element) + 1);
        }
        else{
            repeticiones.set(element, 1);
        }
    });

    // A la función sort, le pasamos una función de comparación propia para ordenar en función de los values del Map
    const ordenados = new Map([...repeticiones.entries()].sort((a, b) => b[1] - a[1]));

    // Iteramos sobre las keys del Map y nos quedamos con el mínimo entre 3 y el tamaño del Map
    const top = [];
    const iterator = ordenados.keys();
    for(let i = 0; i < ordenados.size && i < 3; i++){
        top.push(iterator.next().value);
    }

    return top;
}

/**
 * TEST Ejercicio 2
 */
numbersTop([3, 3, 1, 4, 1, 3, 1, 1, 2, 2, 2, 3, 1, 3, 4, 1]); // [ 1, 3, 2 ]
numbersTop(['a', 3, 2, 'a', 2, 3, 'a', 3, 4, 'a', 'a', 1, 'a', 2, 'a', 3]); // [ 'a', 3, 2 ]

/**
 * TEST propios Ejercicio 2
 */
numbersTop([1,1,1,1,1,1,1,2,2,2,2,2,2]); // [ 1, 2 ]
numbersTop([]); // [ ]