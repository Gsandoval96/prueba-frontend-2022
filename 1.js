// Ejercicio 1
// Dada una matriz de N elementos en la que todos los elementos son iguales excepto uno,
// crea una función findUniq que retorne el elemento único.

/**
 * Devuelve el primer elemento que aparece una única vez en un array. Si no hay un elemento único, devuelve null.
 *
 * @param {Array} array El array a comprobar.
 * @return {unico} Primer elemento único del array, null si no existe.
 */
function findUniq(array) {
    const duplicados = new Set();
    let unico = null;
    for (let i = 0; i < array.length && unico == null; i++){
        const element = array[i];
        // Si el índice de la última aparición del elemento coincide con el índice actual 
        // y no lo hemos encontrado anteriormente (no está en duplicados), es el elemento único
        if(array.lastIndexOf(element) == i && !duplicados.has(element)){
            unico = element;
        }
        else{
            duplicados.add(element);
        }
    }

    return unico;
}


/**
 * TEST Ejercicio 1
 */
findUniq(['12', 10, '12', 11, 1, 11, 10, '12']); // 1
findUniq(['Capitán América', 'Hulk', 'Deadpool', 'Capitán América', 'Hulk', 'Wonder Woman', 'Deadpool', 'Iron Man', 'Iron Man']); // 'Wonder Woman'

/**
 * TEST propios Ejercicio 1
 */
findUniq([1, 2, 3, 4, 5]); // 1
findUniq([1, 0, 2, 7, 7, 6, 1, 6]); // 0
findUniq([1, 1, 2, 2, 3, 3]); // null